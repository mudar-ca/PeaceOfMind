/*
 * Copyright (C) 2013 Fairphone Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
Modifications (MN 2013-12-16):
- Handle RINGER_MODE_CHANGED_ACTION in onReceive()
*/

package ca.mudar.fairphone.peaceofmind.receiver;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.format.DateUtils;

import ca.mudar.fairphone.peaceofmind.Const;
import ca.mudar.fairphone.peaceofmind.R;
import ca.mudar.fairphone.peaceofmind.dashclock.AtPeaceDashClockExtension;
import ca.mudar.fairphone.peaceofmind.data.PeaceOfMindPrefs;
import ca.mudar.fairphone.peaceofmind.data.PeaceOfMindRun;
import ca.mudar.fairphone.peaceofmind.ui.PeaceOfMindActivity;
import ca.mudar.fairphone.peaceofmind.utils.AlarmManagerHelper;
import ca.mudar.fairphone.peaceofmind.utils.DeviceControllerImplementation;
import ca.mudar.fairphone.peaceofmind.utils.IDeviceController;
import ca.mudar.fairphone.peaceofmind.utils.TimeHelper;
import ca.mudar.fairphone.peaceofmind.widget.WidgetProvider;

public class PeaceOfMindBroadCastReceiver extends BroadcastReceiver {

    private static final String TAG = PeaceOfMindBroadCastReceiver.class.getSimpleName();
    private static final int PEACE_OF_MIND_ON_NOTIFICATION = 0;
    private static final int PEACE_OF_MIND_INTERRUPTED_NOTIFICATION = 1;
    private PeaceOfMindPrefs mCurrentStats;
    private SharedPreferences mSharedPreferences;
    private Context mContext;
    private IDeviceController mDeviceController;

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        setupDeviceController(mContext);

        String action = intent.getAction();
        if (action != null) {
            // obtains the piece of mind data from shared preferences
            mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
            mCurrentStats = PeaceOfMindPrefs.getStatsFromSharedPreferences(mSharedPreferences);

            if (action.equals(Const.PeaceOfMindActions.UPDATE_PEACE_OF_MIND)) {
                updateDuration(intent);
            } else if (action.equals(Const.PeaceOfMindActions.END_PEACE_OF_MIND)) {
                endPeaceOfMind(false);
            } else if (action.equals(Const.PeaceOfMindActions.INTERRUPT_PEACE_OF_MIND)) {
                // This is called to end PoM when user has toggled mode in Settings
                endPeaceOfMind(true, DeviceControllerImplementation.getInverseDeviceController(context));
            } else if (Intent.ACTION_SHUTDOWN.equals(action) && mCurrentStats.mIsOnPeaceOfMind) {
                endPeaceOfMind(true);
            } else if (Intent.ACTION_AIRPLANE_MODE_CHANGED.equals(action) || AudioManager.RINGER_MODE_CHANGED_ACTION.equals(action)) {
                if (mCurrentStats.mIsOnPeaceOfMind) {
                    // Only react to this if the app is running
                    final boolean hasAirplaneMode = PeaceOfMindPrefs.hasAirplaneMode(PreferenceManager.getDefaultSharedPreferences(context));
                    if (Intent.ACTION_AIRPLANE_MODE_CHANGED.equals(action) && hasAirplaneMode) {
                        final Bundle extras = intent.getExtras();
                        // if the intent was sent by the system end Peace of mind
                        if (!extras.containsKey(Const.PeaceOfMindIntents.EXTRA_TOGGLE)) {
                            endPeaceOfMind(true);
                        }
                    } else if (AudioManager.RINGER_MODE_CHANGED_ACTION.equals(action)) {
                        final AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                        if (!hasAirplaneMode) {
                            // Sound settings were changed while running PoM in SilentMode,
                            // so we interrupt PeaceOfMind
                            if (audioManager.getRingerMode() != AudioManager.RINGER_MODE_SILENT) {
                                endPeaceOfMind(true);
                            }
                        } else {
                            // Sound settings were changed while running PoM in AirplaneMode,
                            if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT) {
                                // Device was set to SilentMode, so we need to check it's not the system's call following
                                // the start of PoM
                                final long currentTime = System.currentTimeMillis();
                                if (mCurrentStats.mCurrentRun.mStartTime - currentTime > DateUtils.MINUTE_IN_MILLIS) {
                                    // PoM was started over a minute ago (rounded values), so it's user interaction
                                    updateRingerRestoreMode();

                                    /**
                                     * Note: this is not very clean, but it also allows users to define the sound mode
                                     * they want to restore to after PoM Silent mode, if they do it during the first minute!
                                     */
                                }
                            } else {
                                // Otherwise, we save the new Sound settings to restore them at end of PoM
                                updateRingerRestoreMode();
                            }
                        }
                    }
                }
            } else if (!action.equals(Const.PeaceOfMindActions.WIDGET_TIMER_TICK)) {
                // No need to call updateWidget()
                return;
            }

            // update the widgets
            updateWidget(context);
        }
    }

    private void setupDeviceController(Context context) {
        mDeviceController = DeviceControllerImplementation.getDeviceController(context);
    }

    private void updateWidget(Context context) {
        if (Const.SUPPORTS_JELLY_BEAN_MR1) {
            AtPeaceDashClockExtension.updateDashClock(context);
        }

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        int[] appWidgetIds = appWidgetManager.getAppWidgetIds(new ComponentName(context, WidgetProvider.class));
        if (appWidgetIds.length > 0) {
            new WidgetProvider().onUpdate(context, appWidgetManager, appWidgetIds);
        }
    }

    private void startPeaceOfMind(long duration) {
        final long currentTime = TimeHelper.getRoundedCurrentTimeMillis();

        AlarmManagerHelper.enableAlarm(mContext.getApplicationContext(), duration + currentTime);
        updateRingerRestoreMode();

        mCurrentStats.mIsOnPeaceOfMind = true;

        mCurrentStats.mCurrentRun = new PeaceOfMindRun();
        mCurrentStats.mCurrentRun.mStartTime = currentTime;
        mCurrentStats.mCurrentRun.mTargetTime = currentTime + duration;
        mCurrentStats.mCurrentRun.mDuration = duration;

        PeaceOfMindPrefs.saveToSharedPreferences(mCurrentStats, mSharedPreferences);

        mDeviceController.startPeaceOfMind();

        // send broadcast to application receiver
        Intent intent = new Intent(PeaceOfMindApplicationBroadcastReceiver.PEACE_OF_MIND_STARTED);
        intent.putExtra(PeaceOfMindApplicationBroadcastReceiver.PEACE_OF_MIND_DURATION, duration);
        mContext.sendBroadcast(intent);
    }

    private void updateDuration(Intent intent) {

        long newDuration = intent.getExtras().getLong(Const.BROADCAST_DURATION_PEACE_OF_MIND);

        if (newDuration == 0) {
            if (mCurrentStats.mIsOnPeaceOfMind) {
                endPeaceOfMind(false);
            }
        } else if (mCurrentStats.mIsOnPeaceOfMind) {
            final long currentTime = System.currentTimeMillis();
            if (currentTime - mCurrentStats.mCurrentRun.mStartTime < newDuration) {
                final long currentTimeRounded = TimeHelper.getRoundedCurrentTimeMillis();

                mCurrentStats.mCurrentRun.mDuration = newDuration;
                mCurrentStats.mCurrentRun.mTargetTime = mCurrentStats.mCurrentRun.mStartTime + newDuration;

                PeaceOfMindPrefs.saveToSharedPreferences(mCurrentStats, mSharedPreferences);

                Intent updateIntent = new Intent(PeaceOfMindApplicationBroadcastReceiver.PEACE_OF_MIND_UPDATED);
                updateIntent.putExtra(PeaceOfMindApplicationBroadcastReceiver.PEACE_OF_MIND_DURATION, mCurrentStats.mCurrentRun.mDuration);
                updateIntent.putExtra(PeaceOfMindApplicationBroadcastReceiver.PEACE_OF_MIND_PAST_TIME, currentTimeRounded - mCurrentStats.mCurrentRun.mStartTime);

                mContext.sendBroadcast(updateIntent);

                AlarmManagerHelper.updateAlarm(mContext, mCurrentStats.mCurrentRun.mTargetTime);
            } else {
                endPeaceOfMind(false);
            }
        } else {
            startPeaceOfMind(newDuration);
            setPeaceOfMindIconInNotificationBar(true, false);
        }
    }

    /**
     * Sets the Peace of mind icon on the notification bar
     *
     * @param putIcon        if true the icon is put otherwise it is removed
     * @param wasInterrupted when true, an extra notification is sent to inform the user that Peace of mind was ended
     */
    private void setPeaceOfMindIconInNotificationBar(boolean putIcon, boolean wasInterrupted) {

        NotificationManager manager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if (putIcon) {

            //just in case the user didn't clear it
            manager.cancel(PEACE_OF_MIND_INTERRUPTED_NOTIFICATION);

            NotificationCompat.Builder builder =
                    new NotificationCompat.Builder(mContext)
                            .setSmallIcon(R.drawable.ic_notify)
                            .setContentTitle(mContext.getResources().getString(R.string.app_name))
                            .setContentText(mContext.getResources().getString(R.string.peace_on_notification));

            Intent resultIntent = new Intent(mContext, PeaceOfMindActivity.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(mContext);
            // Adds the back stack for the Intent (but not the Intent itself)
            stackBuilder.addParentStack(PeaceOfMindActivity.class);
            // Adds the Intent that starts the Activity to the top of the stack
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);

            builder.setContentIntent(resultPendingIntent);

            Notification notificationWhileRunnig = builder.build();
            notificationWhileRunnig.flags |= Notification.FLAG_NO_CLEAR;
            // Add notification
            manager.notify(PEACE_OF_MIND_ON_NOTIFICATION, notificationWhileRunnig);

        } else {
            manager.cancel(PEACE_OF_MIND_ON_NOTIFICATION);

            //send a notification saying that the peace was ended
            if (wasInterrupted) {
                NotificationCompat.Builder builder =
                        new NotificationCompat.Builder(mContext)
                                .setSmallIcon(R.drawable.ic_notify)
                                .setAutoCancel(true)
                                .setContentTitle(mContext.getResources().getString(R.string.app_name))
                                .setContentText(mContext.getResources().getString(R.string.peace_off_notification))
                                .setTicker(mContext.getResources().getString(R.string.peace_off_notification));

                manager.notify(PEACE_OF_MIND_INTERRUPTED_NOTIFICATION, builder.build());
            }
        }
    }

    private void endPeaceOfMind(boolean wasInterrupted) {
        endPeaceOfMind(wasInterrupted, mDeviceController);
    }

    private void endPeaceOfMind(boolean wasInterrupted, IDeviceController deviceController) {

        mCurrentStats.mIsOnPeaceOfMind = false;

        if (mCurrentStats.mCurrentRun != null) {
            mCurrentStats.mCurrentRun.mStartTime = 0;
            mCurrentStats.mCurrentRun.mDuration = 0;
            mCurrentStats.mCurrentRun.mTargetTime = 0;
            mCurrentStats.mCurrentRun = null;
        }

        PeaceOfMindPrefs.saveToSharedPreferences(mCurrentStats, mSharedPreferences);

        if ( deviceController.equals(mDeviceController)) {
            deviceController.endPeaceOfMind();
        }
        else {
            // This is called to end PoM when user has toggled mode in Settings
            deviceController.forceEndPeaceOfMind();
        }


        Intent endIntent = new Intent(PeaceOfMindApplicationBroadcastReceiver.PEACE_OF_MIND_ENDED);
        mContext.sendBroadcast(endIntent);

        setPeaceOfMindIconInNotificationBar(false, wasInterrupted);

        AlarmManagerHelper.disableAlarm(mContext.getApplicationContext(), wasInterrupted);
    }

    private void updateRingerRestoreMode() {
        final int ringerMode = ((AudioManager) mContext.getSystemService(Context.AUDIO_SERVICE)).getRingerMode();
        PeaceOfMindPrefs.setPreviousRingerMode(ringerMode, mSharedPreferences);
    }
}