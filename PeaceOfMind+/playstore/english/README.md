## Title (50 chars)

Peace of Mind+

## Short description (80 chars)

Disconnect from the world, if only for a moment

## Full description (4000 chars)

An open-source fork of Fairphone’s Peace of Mind app.

We become more conscious of our phones as our lives become increasingly connected. Would you like to disconnect, if only for a moment? Your phone will go silent and disconnect from the world. Choose how long you wish to disconnect (up to 12 hours).

Originally developed by Kwame Corporation for the Fairphone, Peace of Mind+ aims simply to bring this beautiful app to other devices, along with new features.

Peace of Mind+ is an open-source project released under the Apache License version 2.0. Source code is available on GitLab https://gitlab.com/mudar-ca/PeaceOfMind

Disclaimer: This is not the official App, and I’m not related to KwameCorp or the Fairphone project. Credit (and many thanks) go to Fairphone/KwameCorp!
