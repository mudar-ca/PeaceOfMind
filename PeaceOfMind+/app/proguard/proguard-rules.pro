-dontobfuscate

-keep class ca.mudar.fairphone.peaceofmind.ui.binding.** { *; }
-keep class ca.mudar.fairphone.peaceofmind.bus.** { *; }

-dontwarn sun.misc.Unsafe
#-dontwarn com.google.common.collect.MinMaxPriorityQueue
