/*
 * Copyright (C) 2013 Mudar Noufal, PeaceOfMind+
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ca.mudar.fairphone.peaceofmind.ui.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.ContextWrapper
import android.content.Intent
import android.content.SharedPreferences
import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.os.Vibrator
import android.provider.Settings
import androidx.preference.CheckBoxPreference
import androidx.preference.Preference
import androidx.preference.PreferenceCategory
import androidx.preference.PreferenceFragmentCompat
import ca.mudar.fairphone.peaceofmind.Const
import ca.mudar.fairphone.peaceofmind.Const.PrefsNames
import ca.mudar.fairphone.peaceofmind.Const.PrefsValues
import ca.mudar.fairphone.peaceofmind.R
import ca.mudar.fairphone.peaceofmind.util.LogUtils
import ca.mudar.fairphone.peaceofmind.util.NotifManagerHelper
import ca.mudar.fairphone.peaceofmind.util.PermissionsManager
import ca.mudar.fairphone.peaceofmind.util.SuperuserHelper


class SettingsFragment : PreferenceFragmentCompat(),
        SharedPreferences.OnSharedPreferenceChangeListener,
        Preference.OnPreferenceChangeListener {

    private var durationPref: Preference? = null
    private var hasAirplaneModePref: CheckBoxPreference? = null
    private var notificationRingtonePref: Preference? = null
    private var notificationListenerPermsPref: CheckBoxPreference? = null
    private var dndPermsPref: CheckBoxPreference? = null
    private var batteryOptimizationPermsPref: CheckBoxPreference? = null

    companion object {
        fun newInstance(): SettingsFragment {
            return SettingsFragment()
        }
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        preferenceManager.sharedPreferencesName = Const.APP_PREFS_NAME
        preferenceManager.sharedPreferencesMode = Context.MODE_PRIVATE

        addPreferencesFromResource(R.xml.preferences)

        durationPref = findPreference(PrefsNames.MAX_DURATION)
        hasAirplaneModePref = findPreference(PrefsNames.HAS_AIRPLANE_MODE)
        notificationRingtonePref = findPreference(PrefsNames.NOTIFICATION_RINGTONE)
        notificationListenerPermsPref = findPreference(PrefsNames.NOTIFICATION_LISTENER_PERMS)
        dndPermsPref = findPreference(PrefsNames.DND_PERMS)
        batteryOptimizationPermsPref = findPreference(PrefsNames.BATTERY_OPTIMIZATION_PERMS)

        removeUnavailablePreferences()

        setupListeners()
    }

    override fun onResume() {
        super.onResume()

        setupSummaries()
        setupPermissionsStatus()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == Const.RequestCodes.RINGTONE_PICKER) {
            onRingtonePickerResult(resultCode, data)
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onDestroy() {
        super.onDestroy()

        // Remove the listener
        preferenceManager?.sharedPreferences?.unregisterOnSharedPreferenceChangeListener(this)
    }

    /**
     * Implements SharedPreferences.OnSharedPreferenceChangeListener
     */
    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences?, key: String?) {
        when (key) {
            PrefsNames.MAX_DURATION -> {
                durationPref?.summary = getMaxDurationSummary()
                // Trigger onActivityResult, to clip duration if necessary
                activity?.setResult(Activity.RESULT_OK)
            }
            PrefsNames.HAS_AIRPLANE_MODE -> checkRootForAirplaneMode()
            PrefsNames.NOTIFICATION_RINGTONE -> notificationRingtonePref?.summary = getRingtoneSummary()
        }
    }

    override fun onPreferenceTreeClick(preference: Preference?): Boolean {
        val key = preference?.key
            ?: return false

        when (key) {
            PrefsNames.NOTIFICATION_CHANNEL_SETTINGS -> {
                NotifManagerHelper.updateNotifChannelSettings(ContextWrapper(activity))
                return true
            }
            PrefsNames.NOTIFICATION_RINGTONE -> {
                showRingtonePreferenceDialog()
                return true
            }
        }

        return super.onPreferenceTreeClick(preference)
    }

    /**
     * Implements Preference.OnPreferenceChangeListener
     * This allows the checkbox state to change only if permission changed. UI updates onResume()
     */
    override fun onPreferenceChange(preference: Preference?, newValue: Any?): Boolean {
        when (preference?.key) {
            PrefsNames.NOTIFICATION_LISTENER_PERMS -> showNotificationListenerSettingsIfAvailable()
            PrefsNames.DND_PERMS -> PermissionsManager
                    .showNotificationsPolicyAccessSettings(activity)
            PrefsNames.BATTERY_OPTIMIZATION_PERMS -> PermissionsManager
                    .showBatteryOptimizationSettings(activity)
        }

        return false // to stop UI changes
    }

    private fun setupListeners() {
        preferenceManager.sharedPreferences.registerOnSharedPreferenceChangeListener(this)

        notificationListenerPermsPref?.onPreferenceChangeListener = this
        dndPermsPref?.onPreferenceChangeListener = this
        batteryOptimizationPermsPref?.onPreferenceChangeListener = this
    }

    private fun setupSummaries() {
        durationPref?.summary = getMaxDurationSummary()
        notificationRingtonePref?.summary = getRingtoneSummary()
    }

    @SuppressLint("NewApi")
    private fun setupPermissionsStatus() {
        notificationListenerPermsPref?.isChecked = preferenceManager
                .sharedPreferences.getBoolean(PrefsNames.HAS_NOTIFICATION_LISTENER, false)

        if (Const.SUPPORTS_MARSHMALLOW) {
            dndPermsPref?.isChecked = PermissionsManager.checkNotificationsPolicyAccess(ContextWrapper(context))
            batteryOptimizationPermsPref?.isChecked = PermissionsManager.checkBatteryOptimizationWhitelist(ContextWrapper(context))
        }
    }

    private fun getMaxDurationSummary(): CharSequence {
        val duration = preferenceManager.sharedPreferences
                .getString(PrefsNames.MAX_DURATION, PrefsValues.DELAY_DEFAULT)

        return getString(
                when (duration) {
                    PrefsValues.DELAY_FAST -> R.string.prefs_duration_fast
                    PrefsValues.DELAY_MODERATE -> R.string.prefs_duration_moderate
                    PrefsValues.DELAY_SLOW -> R.string.prefs_duration_slow
                    else -> R.string.empty_string
                }
        )
    }

    private fun getRingtoneSummary(): String {
            val path = preferenceManager.sharedPreferences.getString(PrefsNames.NOTIFICATION_RINGTONE, PrefsValues.RINGTONE_SILENT)
            if (path?.isNotEmpty() == true) {
                val ringtone = RingtoneManager.getRingtone(context, Uri.parse(path))
                if (ringtone == null) {
                    // revert to silent ringtone
                    preferenceManager.sharedPreferences
                        .edit()
                        .putString(PrefsNames.NOTIFICATION_RINGTONE, PrefsValues.RINGTONE_SILENT)
                        .apply()
                } else {
                    return ringtone.getTitle(context)
                }
            }
            return resources.getString(R.string.prefs_summary_notification_ringtone_silent)
        }

    @SuppressLint("NewApi")
    private fun showNotificationListenerSettingsIfAvailable() {
        try {
            // This was a hidden action until API 22, but should work on our minSdkVersion 19.
            PermissionsManager.showNotificationListenerSettings(activity)
        } catch (e: Exception) {
            LogUtils.REMOTE_LOG(e)
            val pref = notificationListenerPermsPref
                    ?: return
            pref.isEnabled = false
            pref.summary = resources.getString(R.string.prefs_summary_notification_listener_disabled)
        }
    }

    private fun showRingtonePreferenceDialog() {
        val intent = Intent(RingtoneManager.ACTION_RINGTONE_PICKER).apply {
            val ringtoneUri = preferenceManager.sharedPreferences.getString(PrefsNames.NOTIFICATION_RINGTONE, null)
                ?.let { Uri.parse(it) }

            putExtra(RingtoneManager.EXTRA_RINGTONE_TYPE, RingtoneManager.TYPE_NOTIFICATION)
            putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_SILENT, true)
            putExtra(RingtoneManager.EXTRA_RINGTONE_SHOW_DEFAULT, true)
            putExtra(RingtoneManager.EXTRA_RINGTONE_DEFAULT_URI, Settings.System.DEFAULT_NOTIFICATION_URI)

            // Select current value
            putExtra(RingtoneManager.EXTRA_RINGTONE_EXISTING_URI, ringtoneUri)
        }

        startActivityForResult(intent, Const.RequestCodes.RINGTONE_PICKER)
    }

    private fun onRingtonePickerResult(resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK) {
            val ringtoneUri = data?.getParcelableExtra(RingtoneManager.EXTRA_RINGTONE_PICKED_URI) as? Uri?
            preferenceManager.sharedPreferences
                .edit()
                .putString(PrefsNames.NOTIFICATION_RINGTONE, ringtoneUri?.toString())
                .apply()
        }
    }

    private fun removeUnavailablePreferences() {
        removeRootCategoryIfNotAvailable()
        removeVibrationIfNotSupported()
    }

    private fun removeRootCategoryIfNotAvailable() {
        val isRootAvailable = preferenceManager
                .sharedPreferences.getBoolean(PrefsNames.IS_ROOT_AVAILABLE, false)
        if (!isRootAvailable) {
            preferenceScreen.removePreference(hasAirplaneModePref)

            SuperuserHelper.checkRootAvailability()
        }
    }

    private fun removeVibrationIfNotSupported() {
        val vibrator = context?.applicationContext
            ?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator

        if (!vibrator.hasVibrator()) {
            val notificationVibratePref = (findPreference(PrefsNames.NOTIFICATION_VIBRATE) as? CheckBoxPreference)

            notificationVibratePref?.let {
                notificationVibratePref.isChecked = false
                val parentCategory = findPreference(PrefsNames.CATEGORY_NOTIFICATIONS) as? PreferenceCategory
                parentCategory?.removePreference(notificationVibratePref)
            }
        }
    }

    private fun checkRootForAirplaneMode() {
        val hasAirplaneMode = preferenceManager
                .sharedPreferences.getBoolean(PrefsNames.HAS_AIRPLANE_MODE, false)

        hasAirplaneModePref?.isChecked = hasAirplaneMode
        if (hasAirplaneMode) {
            SuperuserHelper.isAccessGiven()
        }
    }
}
